{
    "id": "d8840c0e-4d55-43ec-ab9b-b635c6a4a36c",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "emulogic3",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Emulogic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f6b69df9-02a1-476b-8bf9-d3439bfde259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "71814a02-c0f6-41a0-b718-6275a512dae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 4,
                "shift": 13,
                "w": 6,
                "x": 164,
                "y": 78
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ad830174-487d-4d4e-84f8-27b54efbdc65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 24,
                "y": 78
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1dd4b793-6092-487b-af8d-4ce2f45b5a9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 59
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "522c87fc-1e94-4b6e-baaa-cf5775c8f10b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 226,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5a825338-684f-41b4-bb53-561d308a406f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8990684b-f4ef-40a4-bbd6-72a4744416f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "07148849-b498-4f4f-b0c1-41c1c082c3b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 4,
                "x": 201,
                "y": 78
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "88d8c297-9b98-4314-b2b1-1130bb374ab8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 130,
                "y": 78
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ce4facd6-4dce-47af-859c-374f32a98893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 4,
                "shift": 13,
                "w": 8,
                "x": 111,
                "y": 78
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a343b658-12b3-49d1-9b15-de553f719267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 57,
                "y": 78
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f27f1aa7-19bd-467c-bd6b-f4b1b6f9d573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 46,
                "y": 78
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b71f1f69-2d7a-46a0-8efd-d8779e97547b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 156,
                "y": 78
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f23c71b2-69fe-46e6-934c-c86460390d06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 35,
                "y": 78
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "dc0b8308-4b86-45e9-b5fa-1cb5c2979e07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 180,
                "y": 78
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1b5a2849-810e-4e3a-8594-a6aa40776e99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "342e28d3-b865-40fe-8821-8621bd92242f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 170,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "71a5c6e9-4151-495a-974d-3574b51715e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 192,
                "y": 59
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "38b48651-2d87-438b-8bd9-205603505607",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "15c8aa7a-0178-409c-a599-d8c44e4a9fbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "59ad345c-6376-4c60-914a-fae4d6c8f62c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 59
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f3fd4d6e-8415-4e45-b4dd-0a55cc3fa2c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8b245b04-8985-42fd-aa80-8aac3153d8c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 40
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f61858d9-d645-491b-bdc7-df1d621db9c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 40
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d1bf6ddb-0f0f-4017-94d7-516d4ad455b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 40
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "eb9d6fa1-0fdc-44b7-9344-3093664719d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 40
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9ae5b74a-de0e-41a5-aae3-00a3c1133868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 187,
                "y": 78
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "65577e2a-737c-486b-8d82-b55860b1806c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 172,
                "y": 78
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4aaa12df-dee8-46b2-aa2f-d188def8c24d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 90,
                "y": 78
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "db0c8a6d-67b2-487d-bea9-ffc79376cbe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 79,
                "y": 78
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f9d2d4f7-c41e-4b7b-8852-90d7d962410c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 68,
                "y": 78
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4a2b1039-2e39-4707-b800-37d50fff205b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e3c2285b-a7ec-46e0-9ccc-60295ea65fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9eaa8ac9-fc9e-47e4-987d-58e546f50ca2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e9b39edb-b5a2-4bfc-bc72-6bf36ff5ed57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3f66ffeb-e77f-4414-b50c-d60dc9a73811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "68cae3ca-c8b5-4e52-8dc5-90f2fa8d8ea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b503436b-2478-4f13-a8fb-a597d4bef875",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 59
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e75beae7-1a8e-4f03-8350-331d907e6c68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 59
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a95b6866-f8c2-48e9-92d3-4a81a9dc99ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 59
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f4de5875-dcb8-4948-8095-b0129a405b75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 212,
                "y": 40
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ffa6d3c1-cc79-4ea5-8cc5-ed2c7dfbf5f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 204,
                "y": 59
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "deef9a9f-6e36-48cc-b492-c10d0b116df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 40
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3e091d1d-d124-425b-8a3f-aef0df6cfb2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 40
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f36696d5-9203-47a9-8ed1-623639410436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e18634a6-79db-49a2-93a3-8c929ce752cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "14289b72-622e-4af4-be63-9841dd8c40b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0b36d502-641a-4ab8-b723-31d21a27d96f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "39aac948-ded7-4c35-9d6c-9a4b0589066b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "cb56a2db-000a-4673-a37e-fc3ce25127d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "02e83d37-2b17-4db2-8509-049d90b3d0aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0717aea4-7432-40cc-bd5e-c5ca861de974",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2156d184-f65c-47f7-919a-3ec385e91104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 228,
                "y": 59
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6f62c840-896b-437c-806d-8b3206e3ecfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1d0d5277-6536-41e5-af7a-afb59c077f45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ab59b269-e0aa-4502-a8c9-1781beda8ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0ae268f9-ec10-40c0-b849-766ebe712714",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6c75e0c4-ffc5-4d4d-a22a-54d7fb7b06e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 216,
                "y": 59
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3d9bb095-7e8d-4d9f-81ad-37ed29a3bc88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "05115e2b-df60-44e2-9a24-ff354c971fa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 4,
                "shift": 13,
                "w": 8,
                "x": 101,
                "y": 78
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3e8da4e7-89c1-4d7d-bb41-bc95818434b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0c09a8aa-7aee-4fc4-9486-f371d5cc9bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 139,
                "y": 78
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9d4b3cbe-9c42-4085-8bcc-5524cc28bdbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 240,
                "y": 59
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5290597e-9861-47bd-b9fe-ee67999d7de9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "fa14b195-47a2-4344-a285-3b77d23de49e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 6,
                "shift": 13,
                "w": 6,
                "x": 148,
                "y": 78
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f16c96a7-53c5-4294-986b-f14ba0cf3a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "cb7f8c9e-0fcb-4817-b5b0-6ead1841d2db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "83935c6d-0e02-4806-8ea3-242395d2c6d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0722c50f-6fdb-4df0-b755-71887a469104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9ea0a359-7860-4f80-807f-3068b4938409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 21
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "103544d6-ff99-4e29-94a7-3904af21a888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 21
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "62addb57-4bcc-48b8-8c70-3889c2433fe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 40
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1eba2556-0b97-4ea2-8762-708a69e1955a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "63d909a7-89a7-404a-918c-92f5df4c3093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 168,
                "y": 59
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "45a02e99-ee96-409f-b83e-86bfae51d201",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 21
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2c059226-2c25-40b0-822c-c44fabd38ab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 226,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6252cd8c-bcad-481d-8f3a-81471d00f898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 212,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c772b40b-51dc-46d7-a199-022efc43c098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2e060925-523c-4dff-9032-0b00810a6605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3946b58e-f116-4f04-84f9-e96bae62fb68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 170,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "478ef3f8-76ff-4593-a0df-d0955466489c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "618540bd-22ff-478d-8759-c524e94f4360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b3ab71a2-88fa-4be6-8312-23c1c15e1808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c733e6a7-30b2-40e9-812d-c20d31b5911f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "556a14dc-64b2-43e0-99fc-7a686142b35b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 156,
                "y": 59
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e0d81573-86a9-4f73-a573-69f684081166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "84bc63da-7601-4f1e-83d4-e302451a415e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "76b8edc2-be69-4e38-9a25-19635762ff0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5b45cef2-87ec-4365-9819-e50d75384b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "881ef834-7c12-43a3-84e7-9abcbaf63dc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 180,
                "y": 59
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4be97c32-055d-434a-9b02-189d1ea0cda2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "88ebddcc-4e06-4009-b53d-72a3a3200398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b1b24443-854f-4c54-8bb7-5c479b33d394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 194,
                "y": 78
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "46390ef7-80cc-4d49-b2f7-9b3620bf4508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 13,
                "y": 78
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "14bf55f3-7d13-46a2-a7d8-639c25a27120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 121,
                "y": 78
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
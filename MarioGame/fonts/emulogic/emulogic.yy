{
    "id": "379382ad-3081-450e-aa93-69f00cca463c",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "emulogic",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Emulogic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4df77cd5-47d7-488c-bbdd-306129ae594e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 34,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1a4b45df-c091-4fff-86a8-295e20e89fb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 34,
                "offset": 10,
                "shift": 27,
                "w": 11,
                "x": 169,
                "y": 146
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8fb38ef9-bb70-43b7-ba50-11e06f4295b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 451,
                "y": 110
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c38a0077-4e4c-412a-81a9-38fb9e21203c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 158,
                "y": 110
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4d21d3b0-cf78-4970-b709-7d31eab14e61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 366,
                "y": 74
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f93dc39f-23de-4830-8d74-0fd743a978d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 392,
                "y": 74
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7487d148-46e3-4fc8-9aec-851f541b4098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 314,
                "y": 74
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "178938fa-60df-49fa-a791-26e58a57ef6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 8,
                "x": 195,
                "y": 146
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "36ef1711-2498-4d76-a405-410200481e0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 15,
                "x": 79,
                "y": 146
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "984cdac0-4d0e-4d19-9496-a107419ab792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 34,
                "offset": 10,
                "shift": 27,
                "w": 14,
                "x": 112,
                "y": 146
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bf6e2422-2540-475b-afeb-0996681ee2eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 491,
                "y": 110
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8fdb4440-ab02-4c85-bca9-2ec9fee662a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 471,
                "y": 110
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a4061648-1b37-489a-95c6-c5ccf077b857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 11,
                "x": 156,
                "y": 146
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6f06ef6e-b24a-4d14-8ecb-f5207f18f23c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 431,
                "y": 110
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ac90457e-0b0c-4e6d-9952-45546f739666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 34,
                "offset": 10,
                "shift": 27,
                "w": 7,
                "x": 205,
                "y": 146
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "81ee1783-d094-4926-8efc-fc9a27d87882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 288,
                "y": 74
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5ec7fbae-b7f6-42f4-a857-f2c5c3535ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 262,
                "y": 74
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0517ca10-47d5-4ebb-a3f5-9bc88023eb10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 21,
                "x": 279,
                "y": 110
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7d8ad07e-a1b4-409b-8e32-951803a2ad3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 210,
                "y": 74
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "afb01806-1109-4b16-807a-1f2446b261df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 158,
                "y": 74
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "07e99e0a-308f-4057-90f6-eb6d7b906b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 184,
                "y": 110
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3d98ee74-9ec9-44ec-ba6d-9c9b4ba80ae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 80,
                "y": 74
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1bcf8169-f180-40ac-87ad-2dc72aeb12e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 106,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "585f7144-c962-4ac2-8c79-00bafb895fb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 132,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f92d726c-5fa7-4884-bf9f-981fe430941f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 184,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f5665681-8bf9-488d-9491-22de00f3a4af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 236,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e1ada68d-3cea-4645-8f05-9f07e8b5102c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 34,
                "offset": 10,
                "shift": 27,
                "w": 7,
                "x": 214,
                "y": 146
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9c5c24be-e9a7-42b4-853d-c39f8639084f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 11,
                "x": 182,
                "y": 146
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f6ab5dc2-041b-446d-ab94-ed481966e5c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 22,
                "y": 146
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0bfbe662-e01a-49a6-b200-3faf0a362fe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 42,
                "y": 146
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "19dfd577-0429-4618-921f-050f32c3baa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8a334e40-38b4-4b09-a8d4-79f52967f533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 418,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3bde4fe9-612e-429a-a594-514f772aab86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 444,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f4d47d51-00f6-4698-8b73-28c2f9edb4a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 470,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a3cf0e7d-aca8-4ea4-9351-7d928bc51217",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "49e7ab75-d08d-4287-955d-e5d6a13869e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 28,
                "y": 110
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2f2128bd-dae9-4e44-8481-eabf5350ac0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 54,
                "y": 110
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "018ccf7d-681a-4256-9e6b-f0878246105b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 80,
                "y": 110
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2178613a-cf42-41cd-b556-8d680cf10a84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 106,
                "y": 110
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d14a8d82-6d0d-4f16-b2c4-d58139199a58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 132,
                "y": 110
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bf304b1f-546c-4df2-b6b7-4579b0d83c4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 340,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ac69b6ac-cf2e-40a5-8685-0edff4f20ec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 21,
                "x": 302,
                "y": 110
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cb7e9a33-d330-45a0-b986-32467b5c2f9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 28,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "05edb884-54b7-4d0f-a0fc-33afe92aaa99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9549b0f8-43e8-4dce-b594-5ceeaeefcec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 421,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e63ca62d-93cf-436a-a2a2-42f351b3d2d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1fd5987a-79a1-4eac-8adb-761832bed5f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 369,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "98f46de5-1ac8-4897-8381-b301a825c22b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 54,
                "y": 74
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4228a701-9e16-42ff-80d0-fc9fedbe9326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 343,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "cb9677d4-de39-42b2-b183-aeb8c34ea8f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 317,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "565c8ba3-2c99-4bf5-b36f-94e24c3b6371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 291,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "370db7b6-f677-47d4-afa7-e033ef4829ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 265,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9e0a7539-4305-4dbe-815b-69ba4878d635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 21,
                "x": 348,
                "y": 110
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2ae5d275-cd0a-49dc-8857-d262c6b97096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 447,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e6d4edea-5785-4148-9df9-43b63e613bd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "535dd78c-f1f0-474c-b3c4-dc8bf32cdcad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e875742c-47c3-44bb-8fe1-e55effce9bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7c0f85a3-eac9-4653-97c4-6439e4383c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 21,
                "x": 325,
                "y": 110
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "fca973c7-d124-4b74-ac3b-5c9232ffded9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9bdc57ac-4fef-4990-815f-bdec48a9712b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 34,
                "offset": 10,
                "shift": 27,
                "w": 14,
                "x": 96,
                "y": 146
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9faea084-62b4-4039-9067-205e97b10d41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "240fe02e-d0bb-42a1-83d4-cccbb437bc0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 15,
                "x": 62,
                "y": 146
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2867e227-c4ec-4a97-8c17-0248b05bf476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 371,
                "y": 110
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "33e57214-bd7a-48a5-ab62-6a0274dd589e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5eb29c2f-9786-4506-8d44-bdeffdeaad29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 34,
                "offset": 13,
                "shift": 27,
                "w": 11,
                "x": 143,
                "y": 146
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "78a6572c-faf9-489a-8e4f-1968a7b4efea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "725bcd3b-0377-48d5-b16a-339effdb3cc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0411cffb-fd43-4911-b85a-4ae3f749d07a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "12510731-3f4a-4660-861c-1e06056a129f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "baeb222f-8895-4ae1-82ee-8bb69f39cf01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2dc3cc0a-f640-4432-b6cb-9cf408265171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 28,
                "y": 38
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e55c76b4-cc61-4c70-b9bd-e9d733d0c27c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 470,
                "y": 38
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0cab7c19-7beb-4689-b9e6-5e583efb8432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 444,
                "y": 38
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "47972370-3d04-4a64-91e1-3aa46aed6dfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 21,
                "x": 233,
                "y": 110
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5a84e9d6-6f06-414e-850a-3b2017b78e12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 418,
                "y": 38
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "cdbee169-7249-41ca-9db9-84d3c38daca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 392,
                "y": 38
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "ee57c2a3-f807-4d81-82c6-9161fae5bc40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 366,
                "y": 38
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7d81072b-7294-44c1-a160-c235e9641451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 340,
                "y": 38
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "df744285-06d6-4ef8-b403-e8b0d0c2bc49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 314,
                "y": 38
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "00c135dc-3b4d-41ab-bcef-e88c62e26e20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 288,
                "y": 38
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d104d83a-7bd0-49cd-bc46-c0468ab31097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 262,
                "y": 38
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f66ab682-f57b-41bc-ac10-3019d9e58e8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 236,
                "y": 38
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0ee96494-3af6-44a9-88ba-52ed72c6bdc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 210,
                "y": 38
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "509aacaa-ad6c-40aa-97dd-ebb4defc742f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 184,
                "y": 38
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "de1c1b89-e1bd-4cf4-a861-063e05a2a9fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 21,
                "x": 210,
                "y": 110
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d09586e1-9541-4762-be2d-ad2ec3aa39f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 158,
                "y": 38
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "16d12632-b9b9-4859-9039-00ae3d7613e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 132,
                "y": 38
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "11c20f2b-606a-497d-8aec-aa2cd8df4928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 106,
                "y": 38
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "299797ea-be3a-42d8-84a8-0927546bb0a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 80,
                "y": 38
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "97034fc8-bf80-4935-bff9-ebb5bf43aa8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 21,
                "x": 256,
                "y": 110
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4b9bcc41-7ab5-460e-8590-e01d2575ecd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 34,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 54,
                "y": 38
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "98a77f1d-fbe0-4c2b-a519-318f68cfc10e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 391,
                "y": 110
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "20090014-0415-42d6-afd8-48dc194576fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 34,
                "offset": 10,
                "shift": 27,
                "w": 7,
                "x": 223,
                "y": 146
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5ebf2a8e-d5ac-44e8-9e9d-302a21176d99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 34,
                "offset": 6,
                "shift": 27,
                "w": 18,
                "x": 411,
                "y": 110
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ec5b2a5f-9fce-4fd0-aa00-a548569768f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 128,
                "y": 146
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
{
    "id": "03e37f4f-34ee-41c1-a1e4-3804bce08096",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "emulogic2",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Emulogic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "32a0170f-58a4-4ee8-bb5a-c216232a6e35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2d4f8228-2922-4d3d-b4a8-01485353dcb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 6,
                "x": 116,
                "y": 117
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1d2646ab-ff45-4a91-ab92-534e3d7c4d53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 216,
                "y": 94
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0c2826f8-1dff-4137-b0c9-bf96b7efb6f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 94
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "32b46853-90ad-4c4a-af3e-8c4bd2efe8b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 71
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a7dac749-83f6-4c31-bbf1-7085cecd7182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 71
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2f9a4ed0-4730-498d-8711-b589110ed61c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 71
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0a58eb61-60ec-409e-986a-cb3704be4404",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 150,
                "y": 117
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e05ba8d3-00da-4ab7-a6c9-e156f3808fd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 60,
                "y": 117
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6d1aa58d-a7e0-4705-81ff-80d7eb510efd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 8,
                "x": 70,
                "y": 117
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e52e5888-a397-4a4a-800e-183d629cc8db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 14,
                "y": 117
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8c0ffe1d-c674-46cf-98b6-a3160d91e691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 2,
                "y": 117
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6a82254a-4191-4844-b1d7-26ee124ca64c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 108,
                "y": 117
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0bf6e795-0cf9-4677-99e8-7f09543ab471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 228,
                "y": 94
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0bf1049c-51ab-4221-99d6-7c8acc866862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 132,
                "y": 117
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "62a2931e-2e73-4414-a251-94375c57c8b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 71
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "97d69939-4266-451d-811f-7a20bf61ad69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 71
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3468337e-b591-4844-adda-c98213d135b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 124,
                "y": 94
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "dd6dda4a-7766-45e1-87cd-d4e6eb959587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 71
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c42f2592-5653-4d09-a90b-05769eae818f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7cef7b16-f8eb-4172-9814-24c304f5fbbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 94
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "baecaaf9-8d2d-429e-b25b-1ce17b1aa8b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b9cd4228-685b-4982-a257-1161330757df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8530e43d-59f9-448d-a245-5d4928048fe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2054bb64-7dc4-4966-aa89-e428a8795e11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ff8690cc-9699-47ef-830a-b3ae8b04ae88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 71
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "adbba047-e6d0-4f1a-ba28-52eeec4ae1a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 138,
                "y": 117
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0cbee666-0275-4821-bea9-6daa66b47d0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 100,
                "y": 117
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f3559051-9fe1-43fd-ba0f-a57c9eaf10b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 38,
                "y": 117
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b07c9c40-93a1-4a65-b6b5-185a5c433f66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 26,
                "y": 117
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2badece7-c2d5-4b2a-ab2b-fceadcc35d8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 240,
                "y": 94
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "851e2431-c091-4b14-a459-5451cc5ca567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8f1e98cd-e480-4407-8aa2-5926975e9d7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a7b14d46-90a3-4c52-b048-20c28bc40827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "15b24b7b-0eda-4b10-8ce7-06b0aed9ccce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "32c428c9-0c3f-4fd1-8abe-a2c2e82aafb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4ba5bb08-4bd3-4f4c-8f4f-2001720d7f8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "930b2d04-b2a1-4e68-bc55-6b5aa2e02bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "63815ee2-3cc5-4553-b48b-29086557fe2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 94
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fe57b7a0-598f-47aa-8ec8-90a2cd40eefd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 94
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8362138e-b932-4c98-95b5-d88b8c9d1feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5120b439-ba17-4ba5-b7d0-22c80b262e95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 138,
                "y": 94
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f57cf11d-e79a-4b67-a5a3-79bc5e8261fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 48
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "40e10ef5-21ed-46b0-bdf8-6f62238c1f06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 48
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e2d6a535-8296-4085-aa72-9b5cb7470a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 25
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f2121695-cc21-4d67-ba9e-5e1863a645bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d167c192-e969-4c87-8def-d10372dd5479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f4cf71be-3cd1-4e41-8a42-b21872abf397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 48
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "62134a8a-c12f-482a-a931-0fb7a8f21aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "01dc4bb3-db47-481b-8795-15b122d12001",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "07bbd7af-ed84-4d0c-9ee2-b76dbc3bcab8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cd61fb74-991f-4a9a-8689-81674368bb22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "960dc7ec-5148-40ea-9029-69f871677c16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 166,
                "y": 94
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "833f960c-7d4a-4886-80e0-138228679806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 25
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "831d0634-f3a6-4f13-a9ab-679ab73bee49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8bff0299-107d-4e38-a697-f236b24538b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "265dcbaf-9137-446f-a689-0380fb308631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "035bdfc3-4b8b-4e35-a4d8-477146e33c93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 152,
                "y": 94
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3d9c2719-603e-4596-af12-f6613a8ca992",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "db4b748d-fc49-4835-88f5-a10303d4047d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 8,
                "x": 80,
                "y": 117
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f72c150d-5c7f-44e9-8e9b-1c7d1d67ff3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ac6c46e0-ef62-4d18-8a3c-b684f571a506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 90,
                "y": 117
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "288eb176-ad6b-4f4d-a214-0cf753a1314f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 180,
                "y": 94
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "96fb27ba-d6d3-451e-ae04-eac88de4d618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9e9c5515-c0ef-4e52-9978-97c954864b5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 8,
                "shift": 16,
                "w": 6,
                "x": 124,
                "y": 117
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "86a36441-2362-42df-a6c9-e50fcc1d47ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d568b1ee-559e-4ede-8a7f-ff600e5ef05f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "26ff9f22-f33e-4edb-a672-184677a0821e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e1a51628-9841-4227-afe6-ebb764424df2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 25
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d0a07659-6615-4b16-a99f-118c9419674a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 25
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8cbb62af-a23c-45b5-ab5e-126c60984362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 25
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fe72a794-7332-4ff3-9811-842484447b63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 48
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "028cf94f-2374-4378-8566-991bb47eaf01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 48
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "13db1d97-c21c-4051-a15a-f1bb4fe81278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 96,
                "y": 94
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c7c27f57-3da7-495c-94a4-98c7829f20d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 48
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c6cec4e5-89ae-4683-8d93-5dab92869e67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 48
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b9ab1f2a-d8fd-44b7-8f07-8bf1789e4655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 48
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4cf14d4c-35ca-410a-a596-f1fe082e8704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 48
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0e07913a-a667-455e-88f3-224e56719114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 48
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f881490b-fe16-4931-b115-7cd3237933cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6de904b3-a76b-4f50-88d5-d9dd93d8bd0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "585d335a-bd04-4611-a882-84d8519f7ea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "02efda2b-13d4-410c-88f4-97b8187b138e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7144d456-9e4e-403b-a5e6-ab309ea91c61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f0680915-47d5-4eb2-ab30-5e76f0eb0ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 82,
                "y": 94
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2c586461-b6d4-479a-b9c5-0c17a672028a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "83cc4051-eb86-4f3b-91f8-fb48179abfee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "58e2e44b-a46d-4599-a3d3-00275eafda9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e39cf451-6abd-4433-a9ba-495062d0f24c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d9d9db6c-8c42-4e90-9f60-518fa81626df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 110,
                "y": 94
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8f1b059c-0961-4035-9062-9efbeb416e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e1860224-84af-42cd-8415-c2573822ec68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 192,
                "y": 94
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "2099412b-cf5a-481e-9fa5-98e1db1ff25e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 144,
                "y": 117
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8be396b9-e469-410c-b0d7-db28af68b7c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 204,
                "y": 94
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b04c69b1-2706-4394-ab5c-1c9d3315472c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 50,
                "y": 117
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
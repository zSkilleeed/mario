{
    "id": "5c8230d7-e392-499a-89ec-e619274e7688",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "emulogicintro",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Emulogic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "84d617e7-b69f-4e8c-b278-838475d62d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 69,
                "offset": 0,
                "shift": 53,
                "w": 53,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "096875c7-bd39-4020-b8b3-0b2141309c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 69,
                "offset": 19,
                "shift": 53,
                "w": 21,
                "x": 75,
                "y": 286
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "45c07f39-555f-4b87-a795-11f48013c062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 34,
                "x": 685,
                "y": 215
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3d3243cf-6709-44b4-ad62-e34054007ea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 149,
                "y": 215
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "32816092-2d85-4052-b987-5c69c84549d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 590,
                "y": 144
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4f57b3a5-9ad8-425f-96de-a8d480483479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 639,
                "y": 144
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3f7ee5cb-3846-4a71-8ef5-7f28e3394c17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 492,
                "y": 144
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d573fd73-e16d-4354-a9e8-3661e6142126",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 14,
                "x": 172,
                "y": 286
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4936b82b-8e66-442c-a648-601dcecb3dc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 27,
                "x": 990,
                "y": 215
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "26419fdf-3cbd-4e21-8c8a-11046965fd3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 69,
                "offset": 19,
                "shift": 53,
                "w": 28,
                "x": 931,
                "y": 215
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9885caf4-44c3-49f7-a541-339ef2a96270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 34,
                "x": 757,
                "y": 215
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bf12cbf5-c076-4080-a24b-e83a1aec9137",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 34,
                "x": 721,
                "y": 215
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "dff09601-a21f-478a-a7d6-55f4849c6c2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 21,
                "x": 52,
                "y": 286
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9214710b-4b8b-4dd2-8e6f-f2888f0c253e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 34,
                "x": 649,
                "y": 215
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f88319fd-6b60-405c-a14b-c9fcd56c4d67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 69,
                "offset": 19,
                "shift": 53,
                "w": 15,
                "x": 121,
                "y": 286
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f409cd6f-638c-431e-9141-55888b0eddb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 443,
                "y": 144
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "843d7fa1-94ef-495b-bf11-0cd8b1dd2939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 394,
                "y": 144
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8d5997ee-f9b4-4f3f-be35-7a1c3480dfca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 40,
                "x": 373,
                "y": 215
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "83e50577-4088-4b19-b0ec-93847fb50f8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 296,
                "y": 144
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "081948b6-851d-47aa-8323-bdfe71c41ea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 198,
                "y": 144
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1dd50f26-c816-4fc3-a8d6-a5b620ca5491",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 198,
                "y": 215
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "aa02d8de-3f9b-4319-b443-abe23f7ff9c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 51,
                "y": 144
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e2c23213-6d65-434b-a30b-85d6392bd8cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 100,
                "y": 144
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f3c2d43d-74e9-4703-9fe8-7fc9854d33f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 149,
                "y": 144
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3985da20-bc28-4e61-8403-72690f48b736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 247,
                "y": 144
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6dca5820-0afd-48b8-843b-0656920461ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 345,
                "y": 144
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e1b25817-f434-4a36-a62a-b98a3242a4dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 69,
                "offset": 19,
                "shift": 53,
                "w": 15,
                "x": 138,
                "y": 286
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ac029a08-c047-4809-ac97-eda0248e244b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 21,
                "x": 98,
                "y": 286
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "beb1cbb5-a27c-4213-b7d8-16cb5aa68f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 34,
                "x": 829,
                "y": 215
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9712a227-58ca-44a5-8da6-958d64e69256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 34,
                "x": 865,
                "y": 215
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f8bc07f9-cb89-41e7-ba7a-7169f4a4f507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 34,
                "x": 793,
                "y": 215
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c298b583-4d88-4a06-9a4c-5605f68b5108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 688,
                "y": 144
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "64c0cbc4-d67c-4fc4-b827-e92394e08e68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 737,
                "y": 144
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "246dd0e5-56a2-44f0-a99f-a47a65fa5e58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 786,
                "y": 144
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "af615dc7-d128-4793-96de-fc5c9b2a8d5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 835,
                "y": 144
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1c10850c-1b43-427a-8569-2988cdf400ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 884,
                "y": 144
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bf17203c-0ab0-477d-8902-a8b357c2ac67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 933,
                "y": 144
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e887f0ca-00a4-44d5-b92f-0fe5a898dd1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 2,
                "y": 215
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "aa6eb702-85a2-4981-909a-f3c1ab7786e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 51,
                "y": 215
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9867c911-9336-4268-82aa-1ab516310f89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 100,
                "y": 215
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9067ad33-264c-49b1-b240-5bdd588f4d1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 541,
                "y": 144
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5da85c42-c558-4f4f-8c86-380be47613e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 40,
                "x": 415,
                "y": 215
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c5f0bb36-0e9b-4d2a-bdf1-cc28a5edfd89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 933,
                "y": 73
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "cd1f2147-6e89-417b-b3bf-95880d8e6d13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 884,
                "y": 73
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "cc2abcd6-548c-4130-8a3b-d0b1ee95cf97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 792,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "bee2fb75-1a32-4b45-85b0-ef15fc72921d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 743,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b7673cbe-ea85-4d6a-8268-3cac3648743b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 694,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2a613160-02f0-4d97-8d28-54aa7c7b2244",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 2,
                "y": 144
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d84282f8-6a55-4c70-b716-ec500b1aa497",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 645,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a73d5c57-3427-4c95-a86a-2cad5cc82ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 596,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "31df57b0-9c1d-48f6-a4ab-53de2c567dcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 547,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "458fcfbe-9c10-4311-a2b2-8ef44695d209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 498,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "96128815-18b1-460e-bff7-adf7af45eb94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 40,
                "x": 499,
                "y": 215
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9aa7fd0f-217b-4b38-bf38-2237ce0fb645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 841,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6b4f8a7e-3197-46de-9600-3c2a700ee338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 449,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "886fe2b7-fd7d-4e04-be76-848a7d91a94b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 351,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6370598f-c98d-480e-8ca6-7ac5104cd514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 302,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7dc323a4-ef90-4ed1-a2ba-a1eeb716b554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 40,
                "x": 457,
                "y": 215
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9ded7fe8-e6fd-42fa-975c-9a6e51737842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 253,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "137eecbb-ba46-440e-a0d5-d46762f5df65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 69,
                "offset": 19,
                "shift": 53,
                "w": 28,
                "x": 901,
                "y": 215
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3e7bbd74-b1c7-4cbc-bc4d-49e9b3090691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7c37b2ec-738d-4574-a1e9-c15db1ab0e47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 27,
                "x": 961,
                "y": 215
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0671691d-ccdf-4d1d-a7c1-c0254069f4f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 34,
                "x": 541,
                "y": 215
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "948b3d79-fd4e-480a-adef-f5bb76d1e065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "234211b1-a9ec-460f-b6fe-aa60a3fe6e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 69,
                "offset": 26,
                "shift": 53,
                "w": 21,
                "x": 29,
                "y": 286
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "395213b5-498c-4321-b131-a5db1f53d0bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5340e86a-8019-4360-9f61-ca2391931a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ca435e80-f630-46b2-9963-bd91a4840def",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 400,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2ac9647e-7f98-47f8-a5ed-5cb4e4de67f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 890,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4b729bc7-7417-4bbe-b4fc-6f065db84efa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 939,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "58b9c508-e704-4f94-881a-03f34fdeb0d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 2,
                "y": 73
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "399ccebc-ca2a-4385-91ed-06bd2b7270f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 835,
                "y": 73
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9a54b325-967d-4294-982a-facd9d89c77a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 786,
                "y": 73
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "921d625c-5f2d-4ea6-9a87-16758551106d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 40,
                "x": 289,
                "y": 215
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "28fceb47-4d69-4683-9da7-a7d14370e1f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 737,
                "y": 73
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3e1b7793-066f-4b25-98a4-7d05d6e0d301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 688,
                "y": 73
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c7b454f5-777f-4c18-b8bc-d48af5a2cb7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 639,
                "y": 73
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f054fd7c-6805-44cc-a71a-8dfe0b29d82c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 590,
                "y": 73
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e135ac6e-0bba-4155-9997-f4712ff3b167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 541,
                "y": 73
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c57f2bbd-176e-45ef-b05a-9863769d5d04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 492,
                "y": 73
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "75c4c4bd-c71f-41dc-96ab-8baf8b8a292c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 443,
                "y": 73
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "38ad6d6f-df4a-4b9e-ba8d-0bdfe45019f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 394,
                "y": 73
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "125f8df6-47a6-4183-a840-f0d8a9c8f09b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 345,
                "y": 73
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "94ebce88-bcab-4a67-8217-85ba55733bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 296,
                "y": 73
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "731e7b1f-6605-4877-a0f6-a5acd715acfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 40,
                "x": 247,
                "y": 215
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "261e601a-82ef-4487-974b-f47d2266005d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 247,
                "y": 73
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "679f88d0-fee0-4fc8-82e0-5fe17ec64803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 198,
                "y": 73
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a595a531-2021-482b-9e36-715f9b49d717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 149,
                "y": 73
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f58f1e92-c73a-4ad4-b8c7-53a012c6544b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 100,
                "y": 73
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5e303787-6177-4a83-a368-8dd3a0398fc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 40,
                "x": 331,
                "y": 215
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1f2f57cb-7157-4b20-b96f-053613f900c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 69,
                "offset": 6,
                "shift": 53,
                "w": 47,
                "x": 51,
                "y": 73
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8d548555-a97e-4ea9-a945-29d5ecb67762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 34,
                "x": 577,
                "y": 215
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "50fab3d8-2e03-4191-9107-59ff9a683f4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 69,
                "offset": 19,
                "shift": 53,
                "w": 15,
                "x": 155,
                "y": 286
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bd3c2057-35b2-492b-800e-42a7c34cc472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 69,
                "offset": 13,
                "shift": 53,
                "w": 34,
                "x": 613,
                "y": 215
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "367a5752-f01c-4c6c-ad28-e54e93eb4da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 69,
                "offset": 3,
                "shift": 32,
                "w": 25,
                "x": 2,
                "y": 286
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 40,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
{
    "id": "8dbd66b2-6619-4490-bd09-df1f4a1ceff2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "c9c7503d-7fcf-40b5-9220-3c0ec9120982",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8dbd66b2-6619-4490-bd09-df1f4a1ceff2"
        },
        {
            "id": "296253fa-5a63-40f3-be68-99cf933175d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8dbd66b2-6619-4490-bd09-df1f4a1ceff2"
        },
        {
            "id": "b70db44a-eb23-4eb4-926a-884f505c194e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "8dbd66b2-6619-4490-bd09-df1f4a1ceff2"
        },
        {
            "id": "61255d5f-bcf3-4167-a376-fbc492db8a9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8dbd66b2-6619-4490-bd09-df1f4a1ceff2"
        },
        {
            "id": "1c8f431d-b0ee-40be-9309-d73d35d570b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9f8783ff-43ee-4669-92c5-b216a56d8f75",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8dbd66b2-6619-4490-bd09-df1f4a1ceff2"
        },
        {
            "id": "bb182c6e-0331-412b-8284-97d357890888",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 6,
            "eventtype": 2,
            "m_owner": "8dbd66b2-6619-4490-bd09-df1f4a1ceff2"
        },
        {
            "id": "ac962824-9d7a-4e36-b7c9-21241af809f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "8dbd66b2-6619-4490-bd09-df1f4a1ceff2"
        }
    ],
    "maskSpriteId": "0815ffa1-40df-4540-840e-42e575165fa3",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0815ffa1-40df-4540-840e-42e575165fa3",
    "visible": true
}
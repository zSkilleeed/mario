{
    "id": "2f613d19-f4b1-4728-8053-d9836491b404",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemie2",
    "eventList": [
        {
            "id": "b26c4ec0-979c-449b-a216-5a6ceac5de69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2f613d19-f4b1-4728-8053-d9836491b404"
        },
        {
            "id": "7945e7b3-0181-4969-8cc0-c38604193e79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f613d19-f4b1-4728-8053-d9836491b404"
        },
        {
            "id": "b4d6ac03-67c1-43a4-9a29-94f6afd74428",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "dcf980c7-3798-4288-a59c-8e3ae5320d7c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f613d19-f4b1-4728-8053-d9836491b404"
        },
        {
            "id": "f530c79c-411d-4fde-8f8a-577d73223edb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "2f613d19-f4b1-4728-8053-d9836491b404"
        },
        {
            "id": "95f17710-65de-4857-b535-797980288b98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "44215ee8-8aa3-4f28-be13-984fdb4d7fd0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f613d19-f4b1-4728-8053-d9836491b404"
        },
        {
            "id": "8bf5b93f-be20-4292-8026-a21c23d37638",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8dbd66b2-6619-4490-bd09-df1f4a1ceff2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f613d19-f4b1-4728-8053-d9836491b404"
        },
        {
            "id": "03572988-b0bd-480c-af53-057c124ce067",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f7aedb8-6cc2-4740-8580-7f88e0f77211",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f613d19-f4b1-4728-8053-d9836491b404"
        },
        {
            "id": "6b14d4a6-f181-4f91-bdd9-4216ebdea7f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2f613d19-f4b1-4728-8053-d9836491b404"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b27e6512-5b25-414a-9276-d8412ca95874",
    "visible": true
}
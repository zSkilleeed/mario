draw_set_halign(fa_left);
draw_set_valign(fa_middle);
draw_set_font(emulogic);
draw_set_color(c_orange);

var yPlus = 30;

var m;
for (m=0;m<array_length_1d(menu);m+=1)
{
    draw_text(x + 120, y + (m * space) + yPlus, string(menu[m]) );
}

// Dibujamos el selector (hay que ponerlo como sprite del obj_menu)
draw_sprite(sprite_index, 0, x + 13 + 60, y + menuPos * space + yPlus);
draw_set_color(c_red);
draw_text(0, 20, "Bernat Relats");
draw_set_color(c_white);
draw_text(x + 20, room_height-120, "©2018 BERNATENDO CO.,LTD");
draw_text(x + 120, room_height-70, "MADE IN SPAIN");
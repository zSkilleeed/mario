{
    "id": "325bb61e-4a66-499e-a402-c949c0151c0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "text_spawner",
    "eventList": [
        {
            "id": "adcef80b-3ab6-4b9b-b387-9005e4ded8fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "325bb61e-4a66-499e-a402-c949c0151c0c"
        },
        {
            "id": "df54cf32-4001-408f-92f8-743828a5a3c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "325bb61e-4a66-499e-a402-c949c0151c0c"
        },
        {
            "id": "c6b754a4-f437-4dd9-8b58-232315a8bab4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 2,
            "m_owner": "325bb61e-4a66-499e-a402-c949c0151c0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
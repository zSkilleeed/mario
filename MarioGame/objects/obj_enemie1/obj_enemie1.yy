{
    "id": "7f7aedb8-6cc2-4740-8580-7f88e0f77211",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemie1",
    "eventList": [
        {
            "id": "d7fcd240-fd52-47c3-9f32-7878f401bb68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7f7aedb8-6cc2-4740-8580-7f88e0f77211"
        },
        {
            "id": "2f5467fe-5a4c-48a0-8f49-d652e15f5285",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f7aedb8-6cc2-4740-8580-7f88e0f77211"
        },
        {
            "id": "2edb4357-2c42-4abf-a5a0-e159ad117ae5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "dcf980c7-3798-4288-a59c-8e3ae5320d7c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7f7aedb8-6cc2-4740-8580-7f88e0f77211"
        },
        {
            "id": "f58a9910-f1d5-46df-880a-d699b6b34f9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "7f7aedb8-6cc2-4740-8580-7f88e0f77211"
        },
        {
            "id": "0b8939ce-a0c8-4a19-8ef8-ade7aed2a227",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "44215ee8-8aa3-4f28-be13-984fdb4d7fd0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7f7aedb8-6cc2-4740-8580-7f88e0f77211"
        },
        {
            "id": "5b784dc6-eed2-4204-a1d3-8e762f58f984",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8dbd66b2-6619-4490-bd09-df1f4a1ceff2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7f7aedb8-6cc2-4740-8580-7f88e0f77211"
        },
        {
            "id": "e936b7ae-d3b5-4074-a935-b27bd220414e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2f613d19-f4b1-4728-8053-d9836491b404",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7f7aedb8-6cc2-4740-8580-7f88e0f77211"
        },
        {
            "id": "cacf56e6-4461-4669-aa8a-fe633f62ede0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7f7aedb8-6cc2-4740-8580-7f88e0f77211"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b27e6512-5b25-414a-9276-d8412ca95874",
    "visible": true
}
{
    "id": "46dddb2f-9a9d-4a38-b3c8-c6280435e3cf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spawner",
    "eventList": [
        {
            "id": "03a5b81b-b143-4c46-ae1b-de75e857a8a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46dddb2f-9a9d-4a38-b3c8-c6280435e3cf"
        },
        {
            "id": "1d0ff7be-828a-4402-a9dc-6757286abb71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "46dddb2f-9a9d-4a38-b3c8-c6280435e3cf"
        },
        {
            "id": "098dd9f8-16db-4b1f-995e-486c348d6f40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "46dddb2f-9a9d-4a38-b3c8-c6280435e3cf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9d607fa9-3489-4476-a23f-33a0da72080c",
    "visible": true
}
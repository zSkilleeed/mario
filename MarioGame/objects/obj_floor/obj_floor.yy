{
    "id": "dcf980c7-3798-4288-a59c-8e3ae5320d7c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_floor",
    "eventList": [
        {
            "id": "36fbcd3e-26f6-418f-b6c8-fbb77a834d84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dcf980c7-3798-4288-a59c-8e3ae5320d7c"
        },
        {
            "id": "1cc382be-1d2e-4f5f-8659-9190ce4c55c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dcf980c7-3798-4288-a59c-8e3ae5320d7c"
        },
        {
            "id": "ecc42804-96b3-4750-9098-5f8fedf6884e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "dcf980c7-3798-4288-a59c-8e3ae5320d7c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3717653c-813d-4271-bf20-5ddd208fcfe7",
    "visible": true
}
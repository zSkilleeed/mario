{
    "id": "f04b2c68-4bd4-4ef9-9d24-6d8dc227c7b6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_title",
    "eventList": [
        {
            "id": "de8c7fd4-5e7a-446e-9eec-3db58cace787",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f04b2c68-4bd4-4ef9-9d24-6d8dc227c7b6"
        },
        {
            "id": "ef607b02-e338-46b1-a386-1690a6bc9cca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9f8783ff-43ee-4669-92c5-b216a56d8f75",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f04b2c68-4bd4-4ef9-9d24-6d8dc227c7b6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1045adee-90bd-4e7e-b19c-0deea2982d70",
    "visible": true
}
{
    "id": "6ccf5ae9-1303-4ded-9764-7a2a2f1b4db0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_intro",
    "eventList": [
        {
            "id": "ccd89b45-1f65-4231-8768-94c537da3028",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6ccf5ae9-1303-4ded-9764-7a2a2f1b4db0"
        },
        {
            "id": "113bbea8-0ea4-40ad-9209-09fe18ccc1b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ccf5ae9-1303-4ded-9764-7a2a2f1b4db0"
        },
        {
            "id": "58802d83-155c-479e-ada6-b6ecd6977d3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 6,
            "eventtype": 2,
            "m_owner": "6ccf5ae9-1303-4ded-9764-7a2a2f1b4db0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
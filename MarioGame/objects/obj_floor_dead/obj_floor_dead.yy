{
    "id": "cf2d118b-d0e8-4ea0-a5ae-273849eba251",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_floor_dead",
    "eventList": [
        {
            "id": "81d3f994-4308-4686-936e-44eeb80adb43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cf2d118b-d0e8-4ea0-a5ae-273849eba251"
        },
        {
            "id": "a9407569-7004-4523-be96-6fcee499ca53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf2d118b-d0e8-4ea0-a5ae-273849eba251"
        },
        {
            "id": "6f700312-82c0-4726-816c-2db8175a5931",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9f8783ff-43ee-4669-92c5-b216a56d8f75",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cf2d118b-d0e8-4ea0-a5ae-273849eba251"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ce56516d-1e1b-4d9c-9a89-cc3474a01836",
    "visible": true
}
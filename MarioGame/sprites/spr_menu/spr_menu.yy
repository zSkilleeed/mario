{
    "id": "863624ad-e563-4ae6-863c-442bbd7dd4e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dc6f206-6ffe-4d89-8b24-49a2139b4c4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "863624ad-e563-4ae6-863c-442bbd7dd4e4",
            "compositeImage": {
                "id": "945acfef-067d-4b10-bd4c-e49b20cdbbef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc6f206-6ffe-4d89-8b24-49a2139b4c4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "123673aa-67bd-492d-9881-632ea6eae917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc6f206-6ffe-4d89-8b24-49a2139b4c4d",
                    "LayerId": "12257eb9-d713-4337-bc25-0e22ef6246a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "12257eb9-d713-4337-bc25-0e22ef6246a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "863624ad-e563-4ae6-863c-442bbd7dd4e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}
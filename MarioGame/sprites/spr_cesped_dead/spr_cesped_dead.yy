{
    "id": "ce56516d-1e1b-4d9c-9a89-cc3474a01836",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cesped_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 55,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7700ac7-0334-4714-81c1-9fc0f27eeeb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce56516d-1e1b-4d9c-9a89-cc3474a01836",
            "compositeImage": {
                "id": "0f023989-7872-4da9-b256-4130ab06308e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7700ac7-0334-4714-81c1-9fc0f27eeeb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84221088-fc07-4bc4-bdba-04fcd897f71a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7700ac7-0334-4714-81c1-9fc0f27eeeb9",
                    "LayerId": "34cdb2a3-95aa-45a6-abf1-8b2ae23be20f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "34cdb2a3-95aa-45a6-abf1-8b2ae23be20f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce56516d-1e1b-4d9c-9a89-cc3474a01836",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 15
}
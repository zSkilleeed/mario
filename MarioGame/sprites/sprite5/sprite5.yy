{
    "id": "e568708b-b211-4452-aac6-edee6ea13f9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 899,
    "bbox_left": 0,
    "bbox_right": 899,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccca7217-1c1d-4da3-84d6-2fd57b320248",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e568708b-b211-4452-aac6-edee6ea13f9b",
            "compositeImage": {
                "id": "e8c3e670-2197-4347-9ec2-bb3d018d88c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccca7217-1c1d-4da3-84d6-2fd57b320248",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e8f614c-2c52-4b46-85df-230607d9b3b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccca7217-1c1d-4da3-84d6-2fd57b320248",
                    "LayerId": "829aa5f1-ee86-4cf3-8fa2-fd5df0eb7153"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "829aa5f1-ee86-4cf3-8fa2-fd5df0eb7153",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e568708b-b211-4452-aac6-edee6ea13f9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "3717653c-813d-4271-bf20-5ddd208fcfe7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cesped",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 55,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2da4f9c-54a0-4a76-862c-4fcdde3f5002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3717653c-813d-4271-bf20-5ddd208fcfe7",
            "compositeImage": {
                "id": "83f59904-e077-4ccd-bfea-8b62d86bc71b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2da4f9c-54a0-4a76-862c-4fcdde3f5002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba4c8178-ab72-4ce4-b5f5-853afa540c87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2da4f9c-54a0-4a76-862c-4fcdde3f5002",
                    "LayerId": "5f052658-030d-4483-896b-7194131e370f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "5f052658-030d-4483-896b-7194131e370f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3717653c-813d-4271-bf20-5ddd208fcfe7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 102,
    "yorig": 100
}
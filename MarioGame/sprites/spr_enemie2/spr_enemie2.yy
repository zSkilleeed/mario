{
    "id": "40e8acec-2a4c-4f3e-8df3-2069a4dce27f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemie2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85b14ff9-ecfd-4273-a085-5228accbb764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40e8acec-2a4c-4f3e-8df3-2069a4dce27f",
            "compositeImage": {
                "id": "a808880d-edfb-4fbb-8dae-22d5bf46644e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85b14ff9-ecfd-4273-a085-5228accbb764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "598fd93a-786f-4ded-a9c1-af87d1612689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85b14ff9-ecfd-4273-a085-5228accbb764",
                    "LayerId": "8c9f081c-8090-48fb-96c0-47d36106fc7d"
                }
            ]
        },
        {
            "id": "19c6c877-405e-4389-b87b-3d2f0b462122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40e8acec-2a4c-4f3e-8df3-2069a4dce27f",
            "compositeImage": {
                "id": "0aca2c78-36bb-451f-8e98-5af192a3cb7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19c6c877-405e-4389-b87b-3d2f0b462122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef87c67f-ef00-4c74-a4d3-16812a358903",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19c6c877-405e-4389-b87b-3d2f0b462122",
                    "LayerId": "8c9f081c-8090-48fb-96c0-47d36106fc7d"
                }
            ]
        },
        {
            "id": "f3dd8980-6592-4e1f-ac47-7005c3f76c42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40e8acec-2a4c-4f3e-8df3-2069a4dce27f",
            "compositeImage": {
                "id": "ea07236c-63af-414f-aca7-ef358eb276ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3dd8980-6592-4e1f-ac47-7005c3f76c42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "764694fe-4825-4a85-8d8c-3c92025e6665",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3dd8980-6592-4e1f-ac47-7005c3f76c42",
                    "LayerId": "8c9f081c-8090-48fb-96c0-47d36106fc7d"
                }
            ]
        },
        {
            "id": "dfa43cc7-4a98-48b8-b54e-0247b2db785f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40e8acec-2a4c-4f3e-8df3-2069a4dce27f",
            "compositeImage": {
                "id": "5ae34801-a7b0-4928-9c4b-2f028db5c32c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa43cc7-4a98-48b8-b54e-0247b2db785f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23574666-bc09-45e9-ae6f-b302a6dbab4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa43cc7-4a98-48b8-b54e-0247b2db785f",
                    "LayerId": "8c9f081c-8090-48fb-96c0-47d36106fc7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "8c9f081c-8090-48fb-96c0-47d36106fc7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40e8acec-2a4c-4f3e-8df3-2069a4dce27f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}
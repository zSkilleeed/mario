{
    "id": "ba32ecab-aac1-40f3-b9cb-f2afcecd4b04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 13,
    "bbox_right": 69,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39a1eaa7-0793-4cbd-820d-05462a2f3366",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba32ecab-aac1-40f3-b9cb-f2afcecd4b04",
            "compositeImage": {
                "id": "0b46203b-0e17-4a22-aa62-731a96642601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39a1eaa7-0793-4cbd-820d-05462a2f3366",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a589eaa-ca3e-43af-9218-18a060cf0361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39a1eaa7-0793-4cbd-820d-05462a2f3366",
                    "LayerId": "04e41a37-812b-493a-9c33-03d76b016463"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "04e41a37-812b-493a-9c33-03d76b016463",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba32ecab-aac1-40f3-b9cb-f2afcecd4b04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 42,
    "yorig": 50
}
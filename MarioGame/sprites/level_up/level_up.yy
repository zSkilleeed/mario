{
    "id": "823e455f-4e43-4548-be2d-1f9cf48623c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "level_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 799,
    "bbox_left": 47,
    "bbox_right": 427,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18afa255-dc61-4f91-a985-26654cf6ae64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "e91d90dd-d415-45a0-b753-99ba3e58d931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18afa255-dc61-4f91-a985-26654cf6ae64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59fa1a4d-bc7c-476a-b78a-1793d39dcaee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18afa255-dc61-4f91-a985-26654cf6ae64",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "274213e2-441e-4e24-9124-8cb02d2b7e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "124eb902-9ad4-4c38-a881-e7b8db498f43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "274213e2-441e-4e24-9124-8cb02d2b7e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2d99223-c8ab-4e8b-be60-e88bec78be0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "274213e2-441e-4e24-9124-8cb02d2b7e3e",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "78c64c19-7672-4b3b-978c-630d55904194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "41f1a602-6687-4d86-9997-98bb09decc0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78c64c19-7672-4b3b-978c-630d55904194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74fa0767-da02-4a8c-a2d9-f4bb6b45ee2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78c64c19-7672-4b3b-978c-630d55904194",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "baafb67c-5906-4b3d-8f83-7df06985a5da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "97302899-2cad-4859-971a-ca8dbc3156b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baafb67c-5906-4b3d-8f83-7df06985a5da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4904d369-877e-4732-9c04-20dbfc61d723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baafb67c-5906-4b3d-8f83-7df06985a5da",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "45820816-a8ef-4e12-a0ee-bdb5a725c713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "aaa028ec-5e95-49d2-bad0-ff6588192e54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45820816-a8ef-4e12-a0ee-bdb5a725c713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22d0ea54-f9e6-4f08-adee-da6f11d47f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45820816-a8ef-4e12-a0ee-bdb5a725c713",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "ed0442b8-fd42-443f-a9a6-090021617cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "fcdd27c6-90f1-407d-b922-e190957fcbba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed0442b8-fd42-443f-a9a6-090021617cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c486c75-977b-4eab-8b8a-9e2a0efe8c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed0442b8-fd42-443f-a9a6-090021617cdd",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "84223e95-dac5-4b47-924f-bcf91051c1f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "77f1e50d-c072-4850-b84b-f41fc4d2bd4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84223e95-dac5-4b47-924f-bcf91051c1f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56734bd9-e670-4808-9629-a95aa7882330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84223e95-dac5-4b47-924f-bcf91051c1f5",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "0ec90919-53b4-415f-bb23-7935159ad724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "9c817abc-5453-4023-b434-b3932ae2e6fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ec90919-53b4-415f-bb23-7935159ad724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a31d6f64-a1b6-4860-9653-0a2ef7aea1bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ec90919-53b4-415f-bb23-7935159ad724",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "0c783c28-1c73-47cb-8610-4ccd7b9f5a4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "8ae49f2b-5f60-4123-80bd-346d9f3eded9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c783c28-1c73-47cb-8610-4ccd7b9f5a4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c09063-a96c-4c6e-873c-e33a6820d04c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c783c28-1c73-47cb-8610-4ccd7b9f5a4f",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "00c0f539-0992-4603-be83-e3c24225f8a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "a90d5975-3659-4227-9879-648bb362e109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c0f539-0992-4603-be83-e3c24225f8a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5ee1299-a358-4736-9847-429d3a41f268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c0f539-0992-4603-be83-e3c24225f8a8",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "3fca21d9-3b26-4861-9ce2-bb022dd6fdf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "459e8045-38e6-4187-9390-97aa4f2eeac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fca21d9-3b26-4861-9ce2-bb022dd6fdf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1b43283-f3c8-483f-b9fc-2fb23f18623c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fca21d9-3b26-4861-9ce2-bb022dd6fdf4",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "2a1231ff-aa11-4116-a95a-0c6da47a6f69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "fbfd925d-521e-4dda-a6d1-5d158ad6b31d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a1231ff-aa11-4116-a95a-0c6da47a6f69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1340b50c-5ecb-4945-9cb9-1a2c8da455d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a1231ff-aa11-4116-a95a-0c6da47a6f69",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "6da63b4e-67f3-40e8-a795-547d60f38987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "afe538ee-5fea-49bb-bbaa-128fc3d63c60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6da63b4e-67f3-40e8-a795-547d60f38987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c179f2e9-83c0-4dd7-a01c-127287eedbba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6da63b4e-67f3-40e8-a795-547d60f38987",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "8bb01b23-817f-4e8f-b573-d1403616f7b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "38b1eff9-6cfe-4db7-b63c-89a01a91295f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb01b23-817f-4e8f-b573-d1403616f7b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bebbaef7-5ef0-433d-b3ab-5ef22eff40a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb01b23-817f-4e8f-b573-d1403616f7b3",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "ce5f46da-9eec-4007-a569-981239d4c802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "ebe7923d-57a7-42bc-9724-ca05d6814aa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce5f46da-9eec-4007-a569-981239d4c802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58f181b5-4e43-46de-b0fb-a5b3baab621d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce5f46da-9eec-4007-a569-981239d4c802",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "7522b127-5dcd-4b9a-82bd-52d323ab8e6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "906a068a-e375-4633-9033-6d0b6a6db4c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7522b127-5dcd-4b9a-82bd-52d323ab8e6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3e4733d-5f46-4d31-bfef-7340ab606b24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7522b127-5dcd-4b9a-82bd-52d323ab8e6f",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "5e561b8b-c8f8-494b-bdd0-891d096d23a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "6a0798c8-ed47-449e-a4e5-7ee9d5be78d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e561b8b-c8f8-494b-bdd0-891d096d23a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69edf736-89d0-4227-a117-cb7c52be15dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e561b8b-c8f8-494b-bdd0-891d096d23a1",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "85c4cc88-1030-4d6b-a02d-decfe76f2d88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "93d610e9-671c-4e54-bf1b-2dd630e51976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85c4cc88-1030-4d6b-a02d-decfe76f2d88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86a7468f-024f-48da-b4a1-5c6b254e0e5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85c4cc88-1030-4d6b-a02d-decfe76f2d88",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        },
        {
            "id": "6b7deaa1-9e83-45ac-9f44-b20f2d9e5202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "compositeImage": {
                "id": "e08fb398-2607-4cdc-9b19-8aac87a4cad1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b7deaa1-9e83-45ac-9f44-b20f2d9e5202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f86a7995-8d74-4e82-b581-bc03b2945727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b7deaa1-9e83-45ac-9f44-b20f2d9e5202",
                    "LayerId": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "5d0e8e5e-d764-48b1-9f5e-87d18d590c11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "823e455f-4e43-4548-be2d-1f9cf48623c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 240,
    "yorig": 400
}
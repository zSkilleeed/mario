{
    "id": "c4d7acff-ddbf-409a-b9d0-7e53025b5805",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controler_collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9d8e3cd-3213-4621-a98e-9a56e3416118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4d7acff-ddbf-409a-b9d0-7e53025b5805",
            "compositeImage": {
                "id": "7ec1fc4a-5948-4baa-ac34-163ec2e1b4ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d8e3cd-3213-4621-a98e-9a56e3416118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f754397-ec08-4072-92a9-b55709d8a162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d8e3cd-3213-4621-a98e-9a56e3416118",
                    "LayerId": "992f6389-fc0c-4c0b-9a67-cc2323d4f006"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "992f6389-fc0c-4c0b-9a67-cc2323d4f006",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4d7acff-ddbf-409a-b9d0-7e53025b5805",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
{
    "id": "0815ffa1-40df-4540-840e-42e575165fa3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 25,
    "bbox_right": 56,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "280fea3e-59b9-4c29-a216-5e7b6833daf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0815ffa1-40df-4540-840e-42e575165fa3",
            "compositeImage": {
                "id": "d8679354-0ead-47bb-8099-50142aa5767f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "280fea3e-59b9-4c29-a216-5e7b6833daf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70bed6cb-3c5b-420b-92e9-e6e99bb76d4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "280fea3e-59b9-4c29-a216-5e7b6833daf9",
                    "LayerId": "1976db3c-ed35-4daf-a5a7-d6875f2d2237"
                }
            ]
        },
        {
            "id": "523a16f5-d0eb-4f52-b4fb-a145930622b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0815ffa1-40df-4540-840e-42e575165fa3",
            "compositeImage": {
                "id": "12f1e4f1-c173-44d6-859c-d7c8301f174c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "523a16f5-d0eb-4f52-b4fb-a145930622b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "592014fe-ea98-488d-8a35-217b8136aaae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "523a16f5-d0eb-4f52-b4fb-a145930622b1",
                    "LayerId": "1976db3c-ed35-4daf-a5a7-d6875f2d2237"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 104,
    "layers": [
        {
            "id": "1976db3c-ed35-4daf-a5a7-d6875f2d2237",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0815ffa1-40df-4540-840e-42e575165fa3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 42,
    "yorig": 50
}
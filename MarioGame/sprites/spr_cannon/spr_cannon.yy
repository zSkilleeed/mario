{
    "id": "9d607fa9-3489-4476-a23f-33a0da72080c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cannon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 138,
    "bbox_left": 13,
    "bbox_right": 190,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e97ed9be-260e-4897-a90b-5a2accf2eb76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d607fa9-3489-4476-a23f-33a0da72080c",
            "compositeImage": {
                "id": "99bc1387-629e-497e-8ee5-521fc3706b99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97ed9be-260e-4897-a90b-5a2accf2eb76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "801e0dda-feb1-4ebc-946d-7d2736d6cf2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97ed9be-260e-4897-a90b-5a2accf2eb76",
                    "LayerId": "63e287c8-3fd1-4826-ba13-6c0239ce9acb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "63e287c8-3fd1-4826-ba13-6c0239ce9acb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d607fa9-3489-4476-a23f-33a0da72080c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 198,
    "xorig": 166,
    "yorig": 67
}
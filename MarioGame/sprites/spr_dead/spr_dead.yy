{
    "id": "65262011-ef4e-495d-a871-9cb41c3e4a6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 25,
    "bbox_right": 56,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c56dd94-52c8-47a1-a5ca-c6720d801414",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65262011-ef4e-495d-a871-9cb41c3e4a6c",
            "compositeImage": {
                "id": "e52eb63c-7170-469c-9389-bfcc168e0220",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c56dd94-52c8-47a1-a5ca-c6720d801414",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0099399f-e764-487b-a327-d0ae00078c51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c56dd94-52c8-47a1-a5ca-c6720d801414",
                    "LayerId": "fadadb94-48ef-427d-b49e-d0232be794b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 104,
    "layers": [
        {
            "id": "fadadb94-48ef-427d-b49e-d0232be794b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65262011-ef4e-495d-a871-9cb41c3e4a6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 42,
    "yorig": 50
}
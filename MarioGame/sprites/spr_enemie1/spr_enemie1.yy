{
    "id": "b27e6512-5b25-414a-9276-d8412ca95874",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemie1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd8c90c4-a527-4daf-9d49-9e156a441268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b27e6512-5b25-414a-9276-d8412ca95874",
            "compositeImage": {
                "id": "05c37c2a-001c-47f6-9dcd-f2bae58e5484",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd8c90c4-a527-4daf-9d49-9e156a441268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "791e3d86-8fb7-4752-95c5-66b605419878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd8c90c4-a527-4daf-9d49-9e156a441268",
                    "LayerId": "a74c0144-bf66-4ee3-ba1a-dee173724d3a"
                }
            ]
        },
        {
            "id": "4ae04698-1b64-43c2-9592-6dad4a028382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b27e6512-5b25-414a-9276-d8412ca95874",
            "compositeImage": {
                "id": "d351337e-7b0e-4d47-9608-52f41a56cc7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ae04698-1b64-43c2-9592-6dad4a028382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1ea1749-a3c1-45cc-80aa-567ddbeadbc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ae04698-1b64-43c2-9592-6dad4a028382",
                    "LayerId": "a74c0144-bf66-4ee3-ba1a-dee173724d3a"
                }
            ]
        },
        {
            "id": "40ead4ed-7760-4f53-ad20-2e4bc1680b2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b27e6512-5b25-414a-9276-d8412ca95874",
            "compositeImage": {
                "id": "1fbdb751-f47e-4327-b3b1-84520397c9fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40ead4ed-7760-4f53-ad20-2e4bc1680b2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8ec128c-6e44-4f8c-9d81-54a565cfced8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40ead4ed-7760-4f53-ad20-2e4bc1680b2f",
                    "LayerId": "a74c0144-bf66-4ee3-ba1a-dee173724d3a"
                }
            ]
        },
        {
            "id": "7781cc3a-30ed-41f1-8936-0d38247fe6d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b27e6512-5b25-414a-9276-d8412ca95874",
            "compositeImage": {
                "id": "3ba3fece-8556-4a97-a57b-6816b3944d62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7781cc3a-30ed-41f1-8936-0d38247fe6d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a9cb5a5-01be-4e9f-a650-7a95aa2a992c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7781cc3a-30ed-41f1-8936-0d38247fe6d8",
                    "LayerId": "a74c0144-bf66-4ee3-ba1a-dee173724d3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "a74c0144-bf66-4ee3-ba1a-dee173724d3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b27e6512-5b25-414a-9276-d8412ca95874",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}
{
    "id": "1527e97a-c0a4-412f-9c9d-b93ef6a13ef7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 19,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7526f125-5ccc-48a7-aa21-6ea23afba021",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1527e97a-c0a4-412f-9c9d-b93ef6a13ef7",
            "compositeImage": {
                "id": "6093cb88-b22b-4b67-adca-34b4110265b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7526f125-5ccc-48a7-aa21-6ea23afba021",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de06d374-12a1-4886-bd6c-36ff66087f4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7526f125-5ccc-48a7-aa21-6ea23afba021",
                    "LayerId": "6a183f48-c230-4819-bb9d-2cb98c5cd2a1"
                }
            ]
        },
        {
            "id": "4bf09562-e844-430d-bd4c-621c64150c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1527e97a-c0a4-412f-9c9d-b93ef6a13ef7",
            "compositeImage": {
                "id": "d252d905-1f8b-4666-bad3-5933458b6e98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bf09562-e844-430d-bd4c-621c64150c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86b6143d-c4c0-4940-b408-feaebd75f084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bf09562-e844-430d-bd4c-621c64150c01",
                    "LayerId": "6a183f48-c230-4819-bb9d-2cb98c5cd2a1"
                }
            ]
        },
        {
            "id": "6d15ad4e-c1d3-460d-a38f-36fa4c71b5fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1527e97a-c0a4-412f-9c9d-b93ef6a13ef7",
            "compositeImage": {
                "id": "e3a368d2-61c9-4214-9105-6560580a7866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d15ad4e-c1d3-460d-a38f-36fa4c71b5fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25772ce2-70ea-4c55-860e-458756785c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d15ad4e-c1d3-460d-a38f-36fa4c71b5fe",
                    "LayerId": "6a183f48-c230-4819-bb9d-2cb98c5cd2a1"
                }
            ]
        },
        {
            "id": "b97efa40-5a38-4169-8d8f-b11bad621555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1527e97a-c0a4-412f-9c9d-b93ef6a13ef7",
            "compositeImage": {
                "id": "042e745e-707b-43b0-8a32-5b30a332f64f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b97efa40-5a38-4169-8d8f-b11bad621555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20502841-e106-46ce-a44a-d18e66a81f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b97efa40-5a38-4169-8d8f-b11bad621555",
                    "LayerId": "6a183f48-c230-4819-bb9d-2cb98c5cd2a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "6a183f48-c230-4819-bb9d-2cb98c5cd2a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1527e97a-c0a4-412f-9c9d-b93ef6a13ef7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 42,
    "yorig": 50
}
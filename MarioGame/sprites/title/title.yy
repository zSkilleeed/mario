{
    "id": "1045adee-90bd-4e7e-b19c-0deea2982d70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffb5c3e2-9965-44af-8e4f-3340925790ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1045adee-90bd-4e7e-b19c-0deea2982d70",
            "compositeImage": {
                "id": "b87a4c24-3758-486d-afbd-d284598d6b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb5c3e2-9965-44af-8e4f-3340925790ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f19e53b6-17d8-444d-aaea-c06ecb0367ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb5c3e2-9965-44af-8e4f-3340925790ab",
                    "LayerId": "bfa05f0c-69ab-44d3-a9ca-ccb8a2666706"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 158,
    "layers": [
        {
            "id": "bfa05f0c-69ab-44d3-a9ca-ccb8a2666706",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1045adee-90bd-4e7e-b19c-0deea2982d70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}
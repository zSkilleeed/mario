move = key_left + key_right;
hsp = move * movespeed;
if(vsp < 10) vsp += grav;
if(place_meeting(x,y+1,obj_floor))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }
 
    //Check if player grounded
    grounded = !key_jump;
   
    vsp = key_jump * -jumpspeed;

}
 
//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}
 
//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 hkp_count=0;
}
 
//Check jumping
if(jumping){
   
    //check if previously we have jump
    if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);      
    }
 
    //don't jump horizontal
    if(hkp_count < hkp_count_small ){
        hsp = 0;
    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{ // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big
    }
	
}
// Horizontal collision
if(place_meeting(x+hsp,y,obj_floor))
{
	while(!place_meeting(x+sign(hsp),y,obj_floor))
	{
		
		x += sign(hsp);	
	}
	
	hsp = 0;
}
// Vertical movement
if(place_meeting(x,y+1,obj_floor))
{
		
	vsp = key_jump * - jumpspeed	
	
		
}

// Vertical collision
if(place_meeting(x,y+vsp,obj_floor))
{
	while(!place_meeting(x,y+sign(vsp),obj_floor))
	{
	
		if(contador < 60){
				y += sign(vsp+10);	
		}else{
			
		y += sign(vsp);	
		}
		

	}


	vsp = 0;
}


if(hsp >= 1){
	sprite_index = spr_run;
	image_xscale = 1;
}
if(hsp <= -1){
	image_xscale = -1;
	sprite_index = spr_run;
}
if(hsp == 0){
	sprite_index = spr_player;
}
if(jumping){
	sprite_index = spr_jump;
}
//Collision Mask on jump
if(place_meeting(x,y-10,obj_floor) && jumping){

	instance_create_layer(obj_player.x,obj_player.y-100 , "Instances", obj_collider_controller);
}else{
	instance_destroy(obj_collider_controller);
}

x += hsp;
y += vsp;
